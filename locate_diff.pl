#!/bin/perl
# This script will perform a global replace of files which contain the block of text specified in your block file
# The block file can contain regex per line (for the whole file)

use POSIX 'strftime';
use File::Basename;
use Getopt::Long qw(GetOptions);
use Digest::MD5 qw(md5 md5_hex md5_base64);
use Cwd qw(cwd);
use File::Touch;

my $usage = <<END;
Usage: $0 [search-dir] [file-search-pattern]
Options:
	--db-file path to locate db /tmp/locate_diff.db, will create if it doesn't exist
	--keep, don't update the db after displaying records, display all new files from here on for every run since the last time the command was run without the --keep option
END

if (@ARGV < 2) {
	print $usage;
	exit 1
}

my $data_dir = '/var/lib/locate_diff';

if (! -e $data_dir) {
    mkdir($data_dir);
}

my $search_dir = $ARGV[0];
if ($search_dir == '') {
    my $search_dir = cwd;
}

$search_dir =~ s/\/$//g;

my $search = $ARGV[1];

my $date_format = '%Y-%m-%d %H:%M:%S';
my $db_file = $data_dir . '/' . md5_base64($search_dir) . '.db';
my $keep = 0;

GetOptions(
	'db-file=s' => \$db_file,
	'keep' => \$keep,
);

my $cache_1 = $db_file . '_cache_1.txt';
my $cache_2 = $db_file . '_cache_2.txt';

# touch cache 2
if ( ! -e $cache_2) {
    touch($cache_2);
}

# update
$res = `updatedb --require-visibility 0 -o "$db_file" -U "$search_dir"`;
if ($? != 0) {
	my $date = strftime $date_format, localtime;
	warn $date . ",ERROR,could not updatedb , '$db_file' $!\n";
	exit(1);
}

# save cache 1 and print
if (-r $db_file) {

	#print "locate -d $db_file $search_dir/$search > $cache_1";
	$res = `locate -d "$db_file" "$search_dir/$search" > $cache_1`;

	my $counter = 0;
	foreach $file (split (/\n/,  `diff -c $cache_2 $cache_1 | grep -E "^\\+"`)) {
		$file =~ s/^\+\s*//g;
		print $file . "\n";
		$counter++;
	}

	if ($counter == 0) {
		exit(2);
	}

}

if ($keep == 1) {
    exit(0);
}

# update again
$res = `updatedb --require-visibility 0 -o "$db_file" -U "$search_dir"`;
if ($? != 0) {
	my $date = strftime $date_format, localtime;
	warn $date . ",ERROR,could not updatedb , '$db_file' $!\n";
	exit(1);
}

# save cache 2 and don't print
if (-r $db_file) {

	$res = `locate -d "$db_file" "$search_dir/$search" > $cache_2`;

}
