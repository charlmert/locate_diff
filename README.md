# Locate Diff
Searches a directory for differences and prints only files that have been added for that script run, the next time the script is run it won't print anything if no files were added to the directory.

## Try it out

1st run initializes the db
```bash
mkdir -p /tmp/files
touch /tmp/files/1.mp3
touch /tmp/files/2.mp3
touch /tmp/files/3.mp3
touch /tmp/files/4.mp3

locate_diff --search-dir /tmp/files *.mp3
```

2nd run will print the differences
```bash
touch /tmp/files/5.mp3
touch /tmp/files/6.mp3
touch /tmp/files/7.mp3
touch /tmp/files/8.mp3

locate_diff --search-dir /tmp/files *.mp3
/tmp/files/5.mp3
/tmp/files/6.mp3
/tmp/files/7.mp3
/tmp/files/8.mp3
```

3rd run will print nothing but exit with code 2, no files added
```bash
locate_diff --search-dir /tmp/files *.mp3
echo $?
2
```

## Setup

Place the file in /usr/local/bin

```bash
cpan install Digest::MD5
cpan install File::Touch
cd /tmp/
wget https://bitbucket.org/charlmert/locate_diff/raw/bf0e6a08ada29a91aec7f9d01a6b1c68a473007c/locate_diff.pl
mv locate_diff.pl /usr/local/bin/locate_diff
chmod +x /usr/local/bin/locate_diff
```

Now just make sure that /usr/local/bin is in your $PATH

```bash
PATH=/usr/local/bin:$PATH
```

## Params
Usage: locate_diff search-dir search-pattern [options]
Options:
	--db-file path to locate db /tmp/locate_diff.db, will create if it doesn't exist
	--keep, don't update the db after displaying records, display all new files from here on for every run since the last time the command was run without the --keep option

## Real World

Directory that needs to chmod o+r to new wav files that enter a directory

Previous implementation took 4 min, 43 seconds to complete

```bash
[root@localhost audio_perms]# time chmod o+r -R /srv/backup/audio/isabella_3_115

real	4m43.067s
user	0m0.619s
sys	0m17.623s
```

New script initialization takes 28 seconds
```bash
[root@localhost audio_perms]# time /usr/local/bin/locate_diff /srv/backup/audio/isabella_3_115 *.wav | xargs -I{} chmod o+r {}

real	0m28.092s
user	0m2.570s
sys	0m0.450s
```

Subsequent run to chmod only the new files take 2 seconds
```bash
[root@localhost audio_perms]# time /usr/local/bin/locate_diff /srv/backup/audio/isabella_3_115 *.wav | xargs -I{} chmod o+r {}

real	0m2.940s
user	0m2.459s
sys	0m0.317s
```

All chmod runs should now take only about 2 seconds per run depending on the amount of new files

## Known Issues

The script does not handle filenames that contain newline characters
